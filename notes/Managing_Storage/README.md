# Managing Storage

- Master Boot Record (MBR)
- Basic Input Output System (BIOS)
- The MBR was defined as the first 512 bytes on a computer hard drive, and
in the MBR an operating system boot loader was
present, as well as a partition table.
- The size that was used for the partition
table was relatively small, just 64 bytes, with the result that in the MBR no
more than four partitions could be created. Since partition size data was
stored in 32-bit values, and a default sector size of 512 bytes was used, the
maximum size that could be used by a partition was limited to 2 TiB
(hardly a problem in the early 1980s).
- GUID Partition Table (GPT)
- Unified Extensible Firmware Interface (UEFI)
- When talking about storage, we use different measurement units. In some
cases, units like megabyte (MB) are used. In other cases, units like
mebibyte (MiB) are used. The difference between these two is that a
megabyte is a multiple of 1,000, and a mebibyte is a multiple of 1,024. In
computers, it makes sense to talk about multiples of 1,024 because that is
how computers address items. However, confusion was created when
hardware vendors a long time ago started referring to megabytes instead of
mebibytes.
- The difference between a kilobyte (KB) and a kibibyte (KiB) is just 24 bytes.
The bigger the numbers grow, the bigger the difference becomes. A
gigabyte, for instance, is 1,000 × 1,000 × 1,000 bytes, so 1,000,000,000
bytes, whereas a gibibyte is 1,024 × 1,024 × 1,024 bytes, which makes a
total of 1,073,741,824 bytes, which is over 70 MB larger than 1 GB.
- The **fdisk** utility has been around for a
long time and is used to create **MBR** partitions. The **gdisk** utility is used to
create **GPT** partitions. 
- **dd if=/dev/sda of=/root/diskfile bs=1M count=1** - create a backup of
the first megabyte of raw blocks and write that to the file
/root/diskfile.
- An extended partition is used only for the purpose of creating
logical partitions. You cannot create file systems directly on an
extended partition!
- The fdisk utility writes changes to disk only when you enter w,
which is the fdisk write command. If you have made a mistake and
want to get out, press q to quit.
- Do not ever use gdisk on a disk that has been formatted with fdisk
and already contains fdisk partitions. gdisk will detect that an MBR
is present, and it will convert this to a GPT (see the following code
listing). Your computer will most likely not be able to boot after
doing this!
- 

