# Scheduling Tasks

- In RHEL 8, tasks can also be scheduled using a Systemd timer.
- The cron service consists of two major components. First, there is the cron
daemon crond. This daemon looks every minute to see whether there is
work to do.
- To monitor the current status of the cron service, you can use the
**systemctl status crond -l** command.
- You don’t need to remember all settings,   man 5 crontab shows all
possible constructions.
- The main configuration file for cron is **/etc/crontab**, but you will not
change this file directly.
