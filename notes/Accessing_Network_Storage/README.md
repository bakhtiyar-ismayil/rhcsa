# Setting up an NFS server is not a part of the RHCSA exam.
However, to practice
your NFS-based skills, it’s useful to set up your own NFS test server. To do so, you
need to go through a few tasks:
1. Create a local directory you want to share.
2. Edit the /etc/exports file to define the NFS share.
3. Start the NFS server.
4. Configure your firewall to allow incoming NFS traffic.

To mount an NFS share, you first need to find the names of the shares. This
information can be provided by the administrator, but it is also possible to find out
yourself. To discover which shares are available, you have multiple options:
- If NFSv4 is used on the server, you can use a root mount. That means that you
just mount the root directory of the NFS server, and under the mount point
you’ll only see the shares that you have access to.
- Use the showmount -e nfsserver command to find out which shares are
available


