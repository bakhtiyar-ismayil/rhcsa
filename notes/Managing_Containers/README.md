# Container

### An example from exam


**sudo yum module install container-tools -y**

**cd /etx/containers**

We have a directory **registeries.conf** and others

We can go to **vim registeries.conf** and change the line if we don't have redhat subscription

registeries=['docker.io']

We can get a hole information from

**man podman**

**man podman-search**

**podman search ubi | less**

Copy line below and paste

**podman pull registery.access.redhat.com/ubi8/ubi**

List podman images

**podman image**

( We can delete all images via command **podman rmi -a**)

**podman search httpd**

**skopio inspect docker://registry.access.redhat.com/rhscl/httpd-24-rhel7 | less**

All are done 
