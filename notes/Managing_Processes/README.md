# Managing Processes

- For everything that happens on a Linux server, a process is started. For
that reason, process management is among the key skills that an
administrator has to master.

- Shell jobs are commands started from the command line. They are
associated with the shell that was current when the process was
started. Shell jobs are also referred to as interactive processes.

- Daemons are processes that provide services. They normally are
started when a computer is booted and often (but certainly not in all
cases) run with root privileges

- Kernel threads are a part of the Linux kernel. You cannot manage them
using common tools, but for monitoring of performance on a system,
it’s important to keep an eye on them.

- When a process is started from a shell, it becomes a child process of that
shell. In process management, the parent-child relationship between
processes is very important. The parent is needed to manage the child. For
that reason, all processes started from a shell are terminated when that
shell is stopped. This also offers an easy way to terminate processes no
longer needed.
Processes started in the background will not be killed when the parent
shell from which they were started is killed. To terminate these processes,
you need to use the kill command, as described later in this chapter.

- Using nohup for this purpose is no longer needed in
RHEL 8. If a parent process is killed while the child process still is
active, the child process becomes a child of systemd instead.

- Kernel threads are a part of the Linux
kernel, and each of them is started with its own process identification
number (PID). When managing processes, you can easily recognize the
kernel processes because they have a name that is between square
brackets.

- . If you are looking for not only the name of the
process but also the exact command that was used to start the process, use
**ps -ef**

-  Alternative ways to use ps exist as well, such
as the command **ps fax**, which shows hierarchical relationships between
parent and child processes

-  Using **ps fax** to Show Parent-Child Relationships Between Processes

-  An alternative way to get the same result **ps -aux | grep dd** is to use the 
**pgrep dd** command.

- By default, all regular processes are equal and are started with the same
priority, which is the priority number 20, as shown by utilities like top. 

- Alternative to **top**

1. htop
2. bpytop 
3. bashtop

-  Use **nice** if you want to start a process with an adjusted
priority. Use **renice** to change the priority for a currently active process.

- **Do not set process priority to –20; it risks blocking other processes
from getting served.** 

1. Use **ps aux | grep dd** to find the PID of the dd command that you
just started. The PID is in the second column of the command output.
2. Use **renice -n 10 -p 1234** (assuming that 1234 is the PID you just
found).

- The signal SIGTERM (15) is used to ask a process to stop.

- The signal SIGKILL (9) is used to force a process to stop.

- The SIGHUP (1) signal is used to hang up a process. The effect is that
the process will reread its configuration files, which makes this a
useful signal to use after making modifications to a process
configuration file.

- To send a signal to a process, you use the kill command

-  You can use **kill -9** to send the SIGKILL signal to the process.

- Use kill -l to show a list of available signals that can be used with
kill.

- There are some commands that are related to kill: **killall** and **pkill**.

- & (used at the end of a command line) - Starts the command immediately in the background.

- Ctrl-Z - Stops the job temporarily so that it can be managed. For instance, it can be moved to the background.

- Ctrl-D -Sends the End Of File (EOF) character to the current
job to indicate that it should stop waiting for further
input.

- bg - Continues the job that has just been frozen using Ctrl-
Z in the background.

- fg - Brings back to the foreground the last job that was
moved to background execution.

- jobs - Shows which jobs are currently running from this
shell. Displays job numbers that can be used as an
argument to the commands bg and fg.

 
 
