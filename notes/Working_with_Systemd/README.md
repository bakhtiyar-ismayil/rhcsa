# Working with Systemd

-  Systemd is also involved in booting your system in
a desired state, which is called a target.

-  To display a list of available units, type **systemctl -t help**

- Unit files can occur in three locations:

1. /usr/lib/systemd/system contains default unit files that have been
installed from RPM packages. You should never edit these files
directly.
2. /etc/systemd/system contains custom unit files. It may also contain
include files that have been written by an administrator or generated
by the systemctl edit command.
3. /run/systemd/system contains unit files that have automatically been
generated.

- If a unit file exists in more than one of these locations, units in the /run
directory have highest precedence and will overwrite any settings that
were defined elsewhere. Units in /etc/systemd/system have second highest
precedence, and units in /usr/lib/systemd/system come last.

- Probably the most important unit type is the service unit

- Use **man systemd.service** for more information 

- A socket creates a method for applications to communicate with one another. A
socket may be defined as a file but also as a port on which Systemd will be
listening for incoming connections.

- **systemctl --type=service** -- Shows only service units

- **systemctl list-units --type=service** -- Shows all active service units 

- **systemctl list-units --type=service --all** -- Shows active and inactive units

- **systemctl --failed --type=service** --Shows all services that have failed  

- **systemctl  status -l <service.name>** -- Shows detailed information about service 

- **systemctl list-dependencies** -- Shows unit dependencies 

- To figure out which options are available for a specific unit, use the **systemctl show**
command.

- **systemctl edit** -- command creates a subdirectory in
/etc/systemd/system for the service that you are editing; for example, if
you use systemctl edit sshd.service, you get a directory with the name
/etc/systemd/systemd/sshd.service.d in which a file with the name
override.conf is created.

- **export SYSTEMD_EDITOR="/bin/vim"** for changing editor


