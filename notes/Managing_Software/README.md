- Software in RHEL is based on Fedora Software. In the versions of Fedora that RHEL 8 is based on, yum
recently has been replaced with the dnf utility. For that reason, it was expected that with the release of
RHEL 8, the yum command would be replaced with the dnf command. Red Hat has decided differently.
With RHEL 8, a new version of yum has been introduced, which is based on the dnf command. You’ll
notice that in many cases, when requesting information about yum, you’re redirected to dnf resources. So
in fact you’re using dnf, but Red Hat has decided to rename it to yum. Even if in reality you’re using the
dnf command, in this book I’m following the Red Hat convention, and for that reason you’ll learn how to
work with yum.

- Before adding the EPEL repository to RHEL, make sure that it doesn’t break your current support status.
EPEL packages are not managed by Red Hat, and adding them may break supported Red Hat packages.

- To tell your server which repository to use, you need to create a file with a name that ends in .repo in the
directory /etc/yum.repos.d. In that file you need the following contents:
1. [label] The .repo file can contain different repositories, each section starting with a label that identifies the
specific repository.
2. name= Use this to specify the name of the repository you want to use.
3. baseurl= This option contains the URL that points to the specific repository location.
 
- When you use a URL, two components are included. First, the URL identifies the protocol to be used and is in
the format protocol://, such as http://, ftp://, or file://. Following the URL is the exact location on that URL. That
can be the name of a web server or an FTP server, including the subdirectory where the files are found. If the
URL is file based, the location on the file system starts with a / as well

- Creating Your Own Repository
1. Insert the installation disk in your virtual machine and make sure it is attached and available.
2. Type mkdir /repo so that you have a mount point where you can mount the ISO file.
3. Add the following line to the end of the /etc/fstab configuration file: /dev/sr0 /repo iso9660 defaults 0 0
4. Type mount -a, followed by mount | grep sr0. You should now see that the optical device is mounted on
the directory /repo. At this point, the directory /repo can be used as a repository.

-  Even if you are using the yum update kernel
command, the kernel package is not updated, but the newer kernel is installed beside the old kernel, so that while
booting you can select the kernel that you want to use.
 
-  Using yum for Package Management:
1. Type yum repolist to show a list of the current repositories that your system is using.
2. Type yum search seinfo. This will give no matching result.
3. Type yum provides /seinfo. The command shows that the setools-console-<version> package contains
this file.
4. Install this package using yum install setools-console. Depending on your current configuration, you
might notice that quite a few dependencies have to be installed also.
5. Type yum list setools-console. You see that the package is listed as installed.
6. Type yum history and note the number of the last yum command you used.
7. Type yum history undo <nn> (where <nn> is replaced with the number that you found in step 6). This
undoes the last action, so it removes the package you just installed.
8. Repeat the yum list setools-console command. The package is now listed as available but not as installed.


