# There are my notes for passing exam 


### 1. Root password 

```
$ rd.break enforcing=0
$ mount -o remount,rw /sysroot
$ chroot /sysroot
$ passwd
$ exit
```
### 2. How to set yum repository 
```
$ dnf config-manager --add-repo <url>
```
### 3. ntp setting
```
$ timedateclt
$ timedatectl set-timezone "Asia/Baku"
$ timedatectl set-ntp yes
$ rpm -qa | grep chrony
$ systemctl status chronyd.service
```

### 4. ifconfig
```
$ vim /etc/sysconfig/network-scripts/ifcfg-et0
```
### 5.adding ip
```
$ nmcli connection show
$ nmcli connection modify System\ eth1 +ipv4.addresses 10.10.5.11/24
$ nmcli connection reload
$ nmcli connection show eth0
```

### 6. ipv4 package forwarding
```
$ vim /etc/sysctl.conf
$ net.ipv4.ip_forward=1
$ cat /proc/sys/net/ipv4/ip_forward
```

### 7. systemctl set-default multi-user.target
```
$ systemctl get-default
$ vim /etc/default/grub
$ GRUB_CMD_LINUX="rhgb quiet = not slience"
$ grub2-mkconfig -o /boot/grub2/grub.cfg
```

### 8. Create a new volume group of 3GB having names as "vgexam"
```
$ lsblk
$ fdisk -l
$ fdisk -l /dev/sdb
$ fdisk /dev/sdb
$ pvcreate /dev/sdb1
$  pvs
$ vgcreate vgexam /dev/sda1
$ vgs
$ pvs
```

### 9. create a new logical volume of 1GB having name as "lvexam" on "vgexam" volume group
```
$ lvcreate -L 1G -n lvexam vgexam
$ lvs
$ lsblk
```

### 10. The "lvexam" logical volume should be formatted with XFS filesystem and mount persistently on the /mnt/lvexam directory
```
$ mkfs.xfs  /dev/vgexam/lvexam
$ lsblk -f
$ mkdir -p /mnt/lvexam
$ vim /etc/fstab
$ UUID="" /mnt/lvexam xfs defaults 0 0
$ mount -a
```

### 11. Extend the XFS filesystem on "lvexam" by 1GB.
```
$ vgs
$ lvs
$ lvextend -L +1G /dev/vgexam/lvexam
$ lvs
$ vgs
$ xfs_growfs /mnt/lvexam
$ df -Th
```

### 12. Use the appropirate utility create a 4TiB this provisioned volume
```
$ umount /dev/sdc
$ lsblk
$ fdisk -l /dev/sdc
$ dnf install -y vdo kmod-kvdo
$ vdo create --name=vdo1 --device=/dev/sdc --vdoLogicalSize=4T --writePolicy=auto --force
$ fdisk -l /dev/mapper/vdo1
```

### 13. Configure a basic web server that displays "Welcome to Baku" once connected. Ensure the firewall allows the http/https services.
```
$ dnf install httpd
$ systemctl start httpd
$ systemctl enable --now httpd
$ cd /var/www/html
$ vim index.html
$ firewall-cmd --list-all
$ firewall-cmd --permanent --add-service={http,https}
$ firewall-cmd reload
$ curl localhost
```

### 14. Find all files that are greather than 4MB in the /etc directory & copy them to /find/largefiles direcroty
```
$ find /etc -type f -size +4M > /find/largefiles
```

### 15. Write a script
```
 #!/bin/bash
 if [ $1 == "redhat"] ; then
 echo "This is redhat distro"
 elif [ $1 == "fedora" ] ; then
 echo "This is a fedora"
 else
 echo "This is neither redhat nor fedora"
 fi
```

### 16. Create user john , lary , lisa , sandy
1. All the new user should have a file named as "welcome" in their respective home directories after account creation 
2. All user passwords should expire after 45 days and there shouls be at least 8 characters in the passwords
3. Users john and lary should be there in "Baku" group , if group doesn't already exist, create it 
4. Users lisa and sandy should be there in "Shusha" group, if doesn't already exist, create it

```
$ cd /etc/skel
$ echo "Welcome to Shusha" > welcome
$ vim /etc/login.defs PASS_MAX, PASS_LEN
$ groupadd Baku
$ groupadd Shusha
$ usermod -a -G Baku sandy
```

### 17. Only the members of the Baku group should have access to the /Baku directory. Make sandy the owner of this directory, make the Baku group owner of this directory
```
$ mkdir /Baku
$ chown sandy:Baku /Baku
$ ls -ld /Baku
$ chmod 770 /Baku
```

### 18. Only the members of the Shusha group should have access to the /Shusha directory. Make john the owner of this directory, make the Shusha group the group owner of this directory.

```
$ mkdir /Shusha
$ chmod 770 /Shusha
$ ls -ld /Shusha
$ chown john:Shusha /Shusha
$ ls -ld /Shusha
```

### 19. The new files should be owned by the group owner and only the file creators should have the permissions to delete their own files. 
```
$ chmod g+s /Baku
$ ls -ld /Baku
$ chmod g+s /Shusha
$ ls-ls /Baku
$ chmod +t /Shusha
$ chmod +t /Baku
$ ls -ld /Shusha
$ ls -ls /Baku
```

### 20. Create a cron job that writes "Landau School" to /var/log/messages file at 01:00 PM at weekdays only. 
```
$ date
$ crontab -i
$ 00 13 * * 1-5 echo "Landau School" >> /var/log/messages
```

### 21. This is a task for **vdo** or **disk creating** and how to slove it 
```
$ lsblk
$ vgcreate rhcsa /dev/nvme0n4
$ lvcreate --type vdo -n rhcsatest1 -l <total PE> -V 50G rhcsa 
$ lsblk
$ mkfs.ext4 -E nodiscard /dev/rhcsa/rhcsatest1 
$ mkdir rhcsatest
$ vim /etc/fstab
** /dev/rhcsa/rhcsatest1 /rhcsatest ext4 defaults 0 0 **
$ mount -a
$ lsblk
```
- DONE 

### 22. Root password RHEL9 

Change line **root=/dev/mapper/rhel-root ro** to **root=/dev/mapper/rhel-root rw** then go to end of this line and type **init=/bin/bash** and paste **ctrl+x** 

```
$ passwd 
$ touch /.autorelable
$ exec /sbin/init 
```

### 23. Create a 2G swap partition which take effect automatically at boot-start, and it should not affect the original swap partition.

```
# fdisk /dev/sdb
# n
# +2G
# w
# partprobe
# free -h
# mkswap /dev/sdb5
# swapon /dev/sdb5
# free -h
# vim /etc/fstab
- /dev/sdb5 swap swap defaults 0 0
# swapon -a
```

### 24. Create a user named landau, and the user id should be 1234, and the password should be thisisapassword.

```
$ useradd -u 1234 landau
$ echo thisisapassword | passwd --stdin landau
```

### 25. Root password another way 

- During boot time when GRUB loader screen is presented press e key. That will open an editor with current kernel boot options.
- Find the line starting with linux16. At the end of that line add rd.break and press Ctrl-x to restart the system with new option.
- For RHCSA8 only - You should also remove existing parameters ro and crashkernel=....
- What this actually does is taking You to the target right at the end of the boot stage - before root filesystem is mounted (on /).
- Type mount -o remount,rw /sysroot. This actually gets You RW access to the filesystem. /sysroot folder is Your normal / hierarchy.
- Type chroot /sysroot to make this folder new root directory.
- Now it is time to change the root password (that is what we are here for right?) - type passwd and provide new password.
- In order to finish the task SELinux must be taken care of. If not, contents of /etc/shadow will be messed up. There are two commands to be provided:

```
 load_policy -i 
 chcon -t shadow_t /etc/shadow
```

and then 

``` 
touch /.autorelabel
```

Type **exit** twice 

- Now You can log into the system using new password

### 26. Create a container database from an image mysql in rhel2 from docker  

DO this as a root user:

```
# sudo yum install container-tools -y 
# systemctl enable --now podman 
# systemctl status podman.service
# systemctl status podman.socket 
# mkdir /podmish 
# chmod o+w  /podmish
# useradd alice 
# passwd alice 
# chown alice:alice  /podmish
# semanage fcontext -a -t container_file_t '/podmish(/.*)?'
# restorecon -Rv /podmish
# firewall-cmd --permanent --add-port=3306/tcp 
# firewall-cmd --reload 
# loginctl enable-linger alice 
```

SSH to Alice user and do below : 

```
$ mkdir -p ~/.config/systemd/user 
$ cd ~/.config/systemd/user/
$ podman run -d --name=mydatabase -v /podmish:/var/lib/mysql:Z -e MYSQL_USER=sandy -e MYSQL_PASSWORD=password -e MYSQL_DATABASE=mydb -e MYSQL_ROOT_PASSWORD=password -p 3306:3306 mariadb:latest 
$ podman ps 
$ podman generate systemd --name mydatabase --files
$ systemctl --user daemon-reload 
$ systemctl --user enable container-mydatabase.service 
$ podman ps 
$ systemctl --user status container-mydatabase.service 
$ podman ps 
$ su - 
# reboot
$ podman ps 
$ cd .config/systemd/user/
$ systemctl --user status container-mydatabase.service 
```
